

# Pet Stair Vacuum Head

This is a vacuum attachment head that fits the Shop-Vac L250 or other
1.25in diameter vacuum hoses.

## Features

- 90-degree rigid triangular body optimized for reaching into corners
  when cleaning stairs and upholsery.
- Rubber short-toothed edge strip inspired by Bissel Pet Hair Eraser
  hand vacuum to pull matted fur out of carpets and off surfaces
  without becoming tangled.

Prints in multiple materials with a snap fit.

## Instructions

### Vacuum Head Body

Print in PETG, PLA, or other rigid plastic.  Optimize for strength
and layer adhesion, as this part will be mechanically stressed.

Print with the large triangular opening down.  Bed adhesion may be
an issue, so you may want a brim, however brims/supports need to be
trimmed carefully, as the triangular opening surface needs to fit
the rubber strip.
Notes

### Rubber Strip

Print in flex material with high layer adhesion, like 95A TPU.

Print with the teeth pointing straight up.  Note that you may need
to manually adjust the bridging angle (PrusaSlicer can select a
terrible angle by default) to create short bridges for TPU to work
well.

### Assembly

Press the vacuum head into the groove in the rubber strip until
it snaps into place.  The fit is very snug and it may require
significant force.  You will feel the edges snap most easily, but
you may need to use a tool to apply pressure at the corners.
Rolling the strip outward at the corners as you press may help
snap it in easier.